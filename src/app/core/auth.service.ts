import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { User } from '../dashboard/users/users.service';



@Injectable()
export class AuthService {
  user: Observable<User>;
  userCreationFirebaseApp;
  currentUser: User;

  constructor(private auth: AngularFireAuth,
              private db: AngularFirestore,
              private router: Router) {

    this.user = this.auth.authState
      .switchMap(user => {
        if (user) {
          return this.db.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return Observable.of(null);
        }
      });
      this.user.subscribe(user => {
        this.currentUser = user;
      });
      this.userCreationFirebaseApp = firebase.initializeApp(environment.firebase, 'userCreation');
  }

  getCurrentUser() {
    return this.currentUser;
  }

  signUpAndAuthUser(email: string, password: string, organization: string) {
    this.auth.auth.createUserAndRetrieveDataWithEmailAndPassword(email, password)
      .then(credential => {
        this.createUserByUID(credential.user, 'owner', organization, credential.user.uid)
          .then(() => {
            this.router.navigate(['/dashboard']);
          })
          .catch(error => {
            console.log('create user by uid (owner user) error', error);
          });
      })
      .catch(error => {
        console.error('owner user sign up with email and password error', error);
      });
  }

  signUpUserWithoutAuth(email: string, password: string, userGroup: string,
    organization: string, organizationId: string) {

    this.userCreationFirebaseApp.auth().createUserWithEmailAndPassword(email, password)
      .then(user => {
        this.userCreationFirebaseApp.auth().signOut();
        this.createUserByUID(user, userGroup, organization, organizationId)
          .then(() => {})
          .catch(error => {
            console.error('user creation error', error);
          });
      })
      .catch(error => {
        console.error('user creation error', error);
        this.userCreationFirebaseApp.auth().signOut();
      });
  }

  createUserByUID(user, userGroup, organization, organizationId) {
    const userDoc = this.db.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      userGroup: userGroup,
      organization: organization,
      organizationId: organizationId
    };
    return userDoc.set(userData);
  }

  signInUserWithEmailAndPassword(email: string, password: string) {
    this.auth.auth.signInAndRetrieveDataWithEmailAndPassword(email, password)
      .then(credential => {
        this.router.navigate(['/dashboard']);
      })
      .catch(error => {
        console.error('user sign in with email and password error', error);
      });
  }

  signOut() {
    this.auth.auth.signOut()
      .then(() => {
        this.router.navigate(['/home']);
      });
  }

}
