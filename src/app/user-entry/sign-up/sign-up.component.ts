import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.signUpForm = this.formBuilder.group({
      userEmail: this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        emailAgain: ['', [Validators.required, Validators.email]]
      }, {
        validator: this.valuesMatchValidator
      }),
      userPassword: this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6)]],
        passwordAgain: ['', [Validators.required, Validators.minLength(6)]]
      }, {
        validator: this.valuesMatchValidator
      })
    });
  }

  // Validator for two matching control values in a form group.
  // Error is returned if the control values do not match.
  // No error (null) is returned if the values match or one of the
  // controls has no value.
  valuesMatchValidator(formGroup: FormGroup) {
    let valueMatch = '';
    for (let key in formGroup.controls) {
      if (formGroup.controls.hasOwnProperty(key)) {
        const control = formGroup.controls[key];
        if (!control.value || control.value.length === 0) {
          // Missing control values can not be compared so no error is returned.
          return null;
        }

        if (valueMatch.length === 0) {
          // The first control's value is stored for later comparison.
          valueMatch = control.value;
        } else if (valueMatch.localeCompare(control.value) !== 0) {
          // Controls values do not match and so error is returned.
          return {
            valuesMatch: {
              error: 'Input values do not match.'
            }
          };
        }
      }
    }
    // Ensure that something (no error) is always returned.
    return null;
  }

  shouldShowControlIsValid(controlName: string) {
    return this.controlIsValid(controlName);
  }

  shouldShowValidationError(controlName: string) {
    return !this.controlIsValid(controlName) && this.controlIsTouched(controlName);
  }

  controlIsValid(controlName: string) {
    return this.signUpForm.get(controlName).valid;
  }

  controlIsTouched(controlName: string) {
    return this.signUpForm.get(controlName).touched;
  }

  onSignUp() {
    const email = this.signUpForm.value.userEmail.email;
    const password = this.signUpForm.value.userPassword.password;
    const organization = 'test-organization';

    this.authService.signUpAndAuthUser(email, password, organization);
  }
}
