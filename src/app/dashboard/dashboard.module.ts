import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRouterModule } from './dashboard-router.module';
import { SharedModule } from '../shared/shared.module';

import { ProjectsService } from './projects/projects.service';
import { UsersService } from './users/users.service';

import { OverviewComponent } from './overview/overview.component';
import { DashboardComponent } from './dashboard.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectCreationComponent } from './projects/project-creation/project-creation.component';
import { ProjectDetailComponent } from './projects/project-detail/project-detail.component';
import { UsersComponent } from './users/users.component';
import { UsersCreationComponent } from './users/users-creation/users-creation.component';
import { ProjectsTableComponent } from './projects/projects-table/projects-table.component';
import { UsersTableComponent } from './users/users-table/users-table.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { TasksTableComponent } from './tasks/tasks-table/tasks-table.component';
import { TasksDetailComponent } from './tasks/tasks-detail/tasks-detail.component';
import { TasksCreationComponent } from './tasks/tasks-creation/tasks-creation.component';
import { TasksService } from './tasks/tasks.service';
import { TasksComponent } from './tasks/tasks.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardRouterModule,
    SharedModule
  ],
  declarations: [
    OverviewComponent,
    DashboardComponent,
    ProjectsComponent,
    ProjectCreationComponent,
    ProjectDetailComponent,
    UsersComponent,
    UsersCreationComponent,
    ProjectsTableComponent,
    UsersTableComponent,
    UserDetailComponent,
    TasksTableComponent,
    TasksDetailComponent,
    TasksCreationComponent,
    TasksComponent
  ],
  exports: [
    DashboardRouterModule
  ],
  providers: [
    ProjectsService,
    UsersService,
    TasksService
  ]
})
export class DashboardModule { }
