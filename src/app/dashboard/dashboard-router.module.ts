import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../core/auth.guard';

import { OverviewComponent } from './overview/overview.component';
import { DashboardComponent } from './dashboard.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectCreationComponent } from './projects/project-creation/project-creation.component';
import { ProjectDetailComponent } from './projects/project-detail/project-detail.component';
import { UsersComponent } from './users/users.component';
import { UsersCreationComponent } from './users/users-creation/users-creation.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { TasksComponent } from './tasks/tasks.component';
import { TasksDetailComponent } from './tasks/tasks-detail/tasks-detail.component';
import { TasksCreationComponent } from './tasks/tasks-creation/tasks-creation.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], children: [
    { path: '', redirectTo: '/dashboard/(dashboard:overview)', pathMatch: 'full' },
    { path: 'overview', component: OverviewComponent, outlet: 'dashboard' },
    { path: 'projects', component: ProjectsComponent, outlet: 'dashboard' },
    // 'create' can not be a child route of 'projects' in a nested router outlet per Angular bug.
    { path: 'projects/create', component: ProjectCreationComponent, outlet: 'dashboard' },
    { path: 'projects/:id', component: ProjectDetailComponent, outlet: 'dashboard' },
    { path: 'projects/:projectId/tasks', component: TasksComponent, outlet: 'dashboard' },
    { path: 'projects/:projectId/tasks/create', component: TasksCreationComponent, outlet: 'dashboard' },
    { path: 'projects/:projectId/tasks/:taskId', component: TasksDetailComponent, outlet: 'dashboard' },
    { path: 'users', component: UsersComponent, outlet: 'dashboard' },
    { path: 'users/create', component: UsersCreationComponent, outlet: 'dashboard' },
    { path: 'users/:uid', component: UserDetailComponent, outlet: 'dashboard' }
  ]}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class DashboardRouterModule { }
