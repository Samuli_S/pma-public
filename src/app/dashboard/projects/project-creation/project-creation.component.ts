import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../../core/auth.service';
import { ProjectsService } from '../projects.service';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'app-project-creation',
  templateUrl: './project-creation.component.html',
  styleUrls: ['./project-creation.component.css']
})
export class ProjectCreationComponent implements OnInit {
  user;

  constructor(private authService: AuthService,
              private projectsService: ProjectsService,
              private router: Router,
              private route: ActivatedRoute) {
    this.user = null;
  }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.user = user;
    });
  }

  onCreateProject(projectForm: NgForm) {
    const projectDetails = projectForm.value;
    this.projectsService.createProject({
      ...projectDetails,
      creatorId: this.user.uid,
      creatorHandle: this.user.email
    })
    .then(() => {
      toast(`Created ${projectDetails.name}`, 4000);
      this.router.navigate(['../'], { relativeTo: this.route });
    })
    .catch(error => {
      toast('Failed to create project', 4000);
      console.error('project creation error', error);
    });
  }
}
