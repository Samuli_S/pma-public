import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentSnapshot } from '@firebase/firestore-types';

import { ProjectsService, Project } from '../projects.service';
import { User } from '../../users/users.service';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../../core/auth.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {
  project: Project;
  user: User;
  userSubscription: Subscription;

  constructor(private projectsService: ProjectsService,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService) {
    this.project = null;
    this.userSubscription = this.authService.user
      .subscribe(user => {
        this.user = user;
      });
  }

  ngOnInit() {
    this.route.paramMap
      .subscribe(params => {
        const projectId = params.get('id');
        if (!projectId) {
          console.error('project detail - no project id');
          this.router.navigate(['../'], { relativeTo: this.route });
        }
        this.projectsService.getProjectById(projectId)
          .then((snapshot: DocumentSnapshot) => {
            if (!snapshot.exists) {
              console.error('project detail - could not find project');
              return;
            }
            const id = snapshot.id;
            const docData = snapshot.data();
            this.project = { id, ...docData } as Project;
          })
          .catch(error => {
            console.error('project detail error', error);
          });
      });
  }

  onNavigateToProjectTasks() {
    this.router.navigate(['/dashboard', { outlets: { dashboard: ['projects', this.project.id, 'tasks'] } }]);
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }
}
