import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { AuthService } from '../../core/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { OrderByDirection } from '@firebase/firestore-types';
import { User } from '../users/users.service';

export interface Project {
  id?: string;
  name: string;
  description: string;
  creatorId: string;
  creatorHandle: string;
  createdAt?: firebase.firestore.FieldValue;
  organizationId?: string;
}

@Injectable()
export class ProjectsService {
  private projectsCollection: AngularFirestoreCollection<Project>;
  selectedProject: Project;
  projects: Observable<Project[]>;
  currentUser: User;

  constructor(private db: AngularFirestore,
              private authService: AuthService) {
    this.currentUser = this.authService.getCurrentUser();
    this.projectsCollection = db.collection<Project>('projects');
    this.selectedProject = null;
    this.projects = this.projectsCollection.snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as Project;
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  getProjectsObservable(): Observable<Project[]> {
    return this.projects;
  }

  listProjects() {
    return this.projectsCollection.ref
      .where('organizationId', '==', this.currentUser.organizationId)
      .get();
  }

  getProjectById(id: string) {
    return this.projectsCollection.doc(id).ref.get();
  }

  listProjectsOrderedBy(propertyName: string, order?: OrderByDirection) {
    if (!order) {
      order = 'asc';
    }
    this.currentUser = this.authService.getCurrentUser();
    return this.projectsCollection.ref
      .where('organizationId', '==', this.currentUser.organizationId)
      .get();
  }

  createProject(project: Project) {
    project.organizationId = this.currentUser.organizationId;
    project.createdAt = firebase.firestore.FieldValue.serverTimestamp();
    return this.projectsCollection.add(project);
  }

}
