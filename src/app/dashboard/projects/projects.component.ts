import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { QuerySnapshot } from '@firebase/firestore-types';
import { Subscription } from 'rxjs/Subscription';

import { ProjectsService, Project } from './projects.service';
import { User } from '../users/users.service';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit, OnDestroy {
  user: User;
  usersSubscription: Subscription;
  projects: any[];
  isLoadingProjects: boolean;
  projectsExist: boolean;

  constructor(private router: Router,
              private route: ActivatedRoute,
              public projectsService: ProjectsService,
              private authService: AuthService) {
    this.projects = [];
    this.isLoadingProjects = true;
    this.projectsExist = true;
  }

  ngOnInit() {
    this.getProjects();
    this.usersSubscription = this.authService.user
      .subscribe(user => {
        this.user = user;
      });
  }

  getProjects() {
    this.projects = [];
    this.isLoadingProjects = true;
    this.projectsService.listProjectsOrderedBy('name')
      .then((snapshot: QuerySnapshot) => {
        if (snapshot.size === 0) {
          this.isLoadingProjects = false;
          this.projectsExist = false;
          return;
        }
        snapshot.docs.forEach(doc => {
          const docData = doc.data();
          const id = doc.id;
          const project = { id, ...docData };
          this.projects.push(project);
          this.isLoadingProjects = false;
        });
      })
      .catch(error => {
        console.error('error getting projects', error);
        this.isLoadingProjects = false;
      });
  }

  onProjectSelected(project: Project) {
    this.projectsService.selectedProject = project;
    this.router.navigate(['/dashboard', { outlets: { dashboard: ['projects', project.id] } }]);
  }

  onNavigateToProjectCreation() {
    this.router.navigate(['/dashboard', { outlets: { dashboard: ['projects', 'create'] } }]);
  }

  ngOnDestroy() {
    this.usersSubscription.unsubscribe();
  }
}
