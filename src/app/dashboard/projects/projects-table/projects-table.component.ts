import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from '../projects.service';

@Component({
  selector: 'app-projects-table',
  templateUrl: './projects-table.component.html',
  styleUrls: ['./projects-table.component.css']
})
export class ProjectsTableComponent implements OnInit {
  @Input() projects: Project[];
  @Output() projectSelected: EventEmitter<Project> = new EventEmitter<Project>();

  constructor() { }

  ngOnInit() {
  }

  onProjectSelected(project: Project) {
    this.projectSelected.emit(project);
  }

  sortProjectsBy(propertyName: string) {
    this.projects = this.projects.sort((a, b) => {
      if (a[propertyName] < b[propertyName]) {
        return -1;
      }
      if (a[propertyName]  > b[propertyName]) {
        return 1;
      }
      return 0;
    });
  }
}
