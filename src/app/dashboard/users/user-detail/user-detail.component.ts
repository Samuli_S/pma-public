import { Component, OnInit } from '@angular/core';
import { UsersService, User } from '../users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentSnapshot } from '@firebase/firestore-types';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  user;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private usersService: UsersService) {
    this.user = null;
  }

  ngOnInit() {
    this.route.paramMap
      .subscribe(params => {
        const userUID = params.get('uid');
        if (!userUID) {
          console.error('uid query parameter was not provided for user');
          this.router.navigate(['../']);
        }
        this.usersService.getUserByUID(userUID)
          .then((snapshot: DocumentSnapshot) => {
            if (!snapshot.exists) {
              console.error('user does not exist for provided uid');
              this.router.navigate(['../']);
            }
            const userData = snapshot.data();
            this.user = { userUID, ...userData };
          })
          .catch(error => {
            console.error('get user by uid error', error);
            this.router.navigate(['../']);
          });
      });
  }

}
