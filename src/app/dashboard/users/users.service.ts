import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { AuthService } from '../../core/auth.service';

export interface User {
  uid?: string;
  email: string;
  userGroup?: string;
  creatorHandle?: string;
  creatorId?: string;
  organization?: string;
  organizationId?: string;
}

@Injectable()
export class UsersService {
  usersCollection: AngularFirestoreCollection<User>;
  currentUser: User;

  constructor(private db: AngularFirestore,
              private authService: AuthService) {
    this.usersCollection = db.collection('users');
    this.currentUser = this.authService.getCurrentUser();
  }

  listUsers() {
    return this.usersCollection.ref
      .where('organizationId', '==', this.currentUser.organizationId).get();
  }

  listUsersByUserGroup(userGroup: string) {
    return this.usersCollection.ref
      .where('userGroup', '==', userGroup)
      .where('organizationId', '==', this.currentUser.organizationId).get();
  }

  getUserByUID(uid: string) {
    return this.usersCollection.doc(uid).ref.get();
  }
}
