import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../../core/auth.service';
import { toast } from 'angular2-materialize';
import { User } from '../users.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-users-creation',
  templateUrl: './users-creation.component.html',
  styleUrls: ['./users-creation.component.css']
})
export class UsersCreationComponent implements OnInit, OnDestroy {
  userForm: FormGroup;
  currentUser: User;
  userSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService) { }

  ngOnInit() {
    this.userSubscription = this.authService.user
      .subscribe(user => {
        this.currentUser = user;
      });
    this.createForm();
  }

  createForm() {
    this.userForm = this.formBuilder.group({
      userEmail: this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        emailAgain: ['', [Validators.required, Validators.email]]
      }, {
        validator: this.valuesMatchValidator
      }),
      userPassword: this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6)]],
        passwordAgain: ['', [Validators.required, Validators.minLength(6)]]
      }, {
        validator: this.valuesMatchValidator
      })
    });
    this.userForm.addControl('userGroup', new FormControl('participant'));
  }

  // Validator for two matching control values in a form group.
  // Error is returned if the control values do not match.
  // No error (null) is returned if the values match or one of the
  // controls has no value.
  valuesMatchValidator(formGroup: FormGroup) {
    let valueMatch = '';
    for (let key in formGroup.controls) {
      if (formGroup.controls.hasOwnProperty(key)) {
        const control = formGroup.controls[key];
        if (!control.value || control.value.length === 0) {
          // Missing control values can not be compared so no error is returned.
          return null;
        }

        if (valueMatch.length === 0) {
          // The first control's value is stored for later comparison.
          valueMatch = control.value;
        } else if (valueMatch.localeCompare(control.value) !== 0) {
          // Controls values do not match and so error is returned.
          return {
            valuesMatch: {
              error: 'Input values do not match.'
            }
          };
        }
      }
    }
    // Ensure that something (no error) is always returned.
    return null;
  }

  shouldShowControlIsValid(controlName: string) {
    return this.controlIsValid(controlName);
  }

  shouldShowValidationError(controlName: string) {
    return !this.controlIsValid(controlName) && this.controlIsTouched(controlName);
  }

  controlIsValid(controlName: string) {
    return this.userForm.get(controlName).valid;
  }

  controlIsTouched(controlName: string) {
    return this.userForm.get(controlName).touched;
  }

  onCreate() {
    const email = this.userForm.value.userEmail.email;
    const password = this.userForm.value.userPassword.password;
    const userGroup = this.userForm.value.userGroup;
    const organization = this.currentUser.organization;
    const organizationId = this.currentUser.organizationId;
    this.authService.signUpUserWithoutAuth(email, password, userGroup, organization, organizationId);

    this.userForm.reset();
    toast(`Created ${email}`, 4000);
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }
}
