import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from '../../projects/projects.service';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit {
  @Input() users: any[];
  @Output() userSelected: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onUserSelected(user) {
    this.userSelected.emit(user);
  }

  sortUsersBy(propertyName: string) {
    this.users = this.users.sort((a, b) => {
      if (a[propertyName] < b[propertyName]) {
        return -1;
      }
      if (a[propertyName]  > b[propertyName]) {
        return 1;
      }
      return 0;
    });
  }
}
