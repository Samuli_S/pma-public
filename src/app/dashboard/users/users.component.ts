import { Component, OnInit } from '@angular/core';
import { UsersService, User } from './users.service';
import { QuerySnapshot } from '@firebase/firestore-types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[];
  isLoadingUsers: boolean;
  usersExist: boolean;

  constructor(private usersService: UsersService,
              private router: Router) {
    this.users = [];
    this.isLoadingUsers = true;
    this.usersExist = true;
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.isLoadingUsers = true;
    this.usersService.listUsers()
      .then((snapshot: QuerySnapshot) => {
        if (snapshot.size === 0) {
          this.isLoadingUsers = false;
          this.usersExist = false;
          return;
        }
        this.users = [];
        snapshot.docs.forEach(userDoc => {
          const userData = userDoc.data();
          this.users.push(userData as User);
        });
        this.isLoadingUsers = false;
      })
      .catch(error => {
        console.error('list users error', error);
        this.isLoadingUsers = false;
      });
  }

  onUserSelected(user: User) {
    this.router.navigate(['/dashboard', { outlets: { dashboard: ['users', user.uid] } }]);
  }

}
