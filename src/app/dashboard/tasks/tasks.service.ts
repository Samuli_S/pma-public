import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { QuerySnapshot } from '@firebase/firestore-types';
import * as firebase from 'firebase/app';
import { AuthService } from '../../core/auth.service';
import { User } from '../users/users.service';

export interface Task {
  id?: string;
  name: string;
  description: string;
  assignedToUID: string;
  assignedToUserHandle: string;
  creatorUID: string;
  creatorUserHandle: string;
  createdAt?: firebase.firestore.FieldValue;
  organizationId?: string;
}

@Injectable()
export class TasksService {
  currentUser: User;

  constructor(private db: AngularFirestore,
              private authService: AuthService) {

    this.currentUser = this.authService.getCurrentUser();
  }

  listTasksByProjectIdAndAssigneeUID(projectId: string, assignedToUID: string) {
    return this.db.collection('projects').doc(projectId).collection('tasks').ref.where('assignedToUID', '==', assignedToUID).get();
  }

  listTasksByProjectId(projectId: string) {
    return this.db.collection('projects').doc(projectId).collection('tasks').ref
      .where('organizationId', '==', this.currentUser.organizationId)
      .get();
  }

  getTaskByProjectIdAndTaskId(projectId: string, taskId: string) {
    return this.db.collection('projects').doc(projectId).collection('tasks').doc(taskId).ref.get();
  }

  createTask(projectId: string, task: Task) {
    task.organizationId = this.currentUser.organizationId;
    task.createdAt = firebase.firestore.FieldValue.serverTimestamp();
    return this.db.collection('projects').doc(projectId).collection('tasks').add(task);
  }
}
