import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../tasks.service';

@Component({
  selector: 'app-tasks-table',
  templateUrl: './tasks-table.component.html',
  styleUrls: ['./tasks-table.component.css']
})
export class TasksTableComponent implements OnInit {
  @Input() tasks: Task[];
  @Output() taskSelected: EventEmitter<Task> = new EventEmitter<Task>();

  constructor() { }

  ngOnInit() {
  }

  onTaskSelected(task: Task) {
    this.taskSelected.emit(task);
  }

  sortTasksBy(propertyName: string) {
    this.tasks = this.tasks.sort((a, b) => {
      if (a[propertyName] < b[propertyName]) {
        return -1;
      }
      if (a[propertyName]  > b[propertyName]) {
        return 1;
      }
      return 0;
    });
  }

}
