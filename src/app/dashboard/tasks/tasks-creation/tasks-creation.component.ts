import { Component, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import {MaterializeAction, toast} from 'angular2-materialize';
import { UsersService, User } from '../../users/users.service';
import { QuerySnapshot } from '@firebase/firestore-types';
import { NgForm } from '@angular/forms';
import { TasksService, Task } from '../tasks.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../core/auth.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-tasks-creation',
  templateUrl: './tasks-creation.component.html',
  styleUrls: ['./tasks-creation.component.css']
})
export class TasksCreationComponent implements OnInit, OnDestroy {
  currentUser;
  userSubscription: Subscription;
  currentProjectId;
  users;
  selectedUser;
  modalActions = new EventEmitter<string|MaterializeAction>();

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService,
              private usersService: UsersService,
              private tasksService: TasksService) {
    this.selectedUser = null;
  }

  ngOnInit() {
    this.userSubscription = this.authService.user
      .subscribe(user => {
        this.currentUser = user;
      });
    this.route.paramMap
      .subscribe(params => {
        this.currentProjectId = params.get('projectId');
      });
    this.getUsers();
  }

  getUsers() {
    this.usersService.listUsersByUserGroup('participant')
      .then((snapshot: QuerySnapshot) => {
        this.users = [];
        snapshot.docs.forEach(userDoc => {
          const userData = userDoc.data();
          this.users.push(userData as User);
        });
      })
      .catch(error => {
        console.error('list users error', error);
      });
  }

  onOpenUsersModal() {
    this.modalActions.emit({action: 'modal', params: ['open']});
  }

  onSelectUser(user: User) {
    this.selectedUser = user;
    this.modalActions.emit({action: 'modal', params: ['close']});
  }

  onCreateTask(taskForm: NgForm) {
    if (!taskForm.valid || !this.currentUser) {
      console.error('invalid task form and/or user is not selected');
      return;
    }
    const value = taskForm.value;
    const task: Task = {
      name: value.name,
      description: value.description,
      assignedToUID: this.selectedUser.uid,
      assignedToUserHandle: this.selectedUser.email,
      creatorUID: this.currentUser.uid,
      creatorUserHandle: this.currentUser.email,
    };
    this.tasksService.createTask(this.currentProjectId, task)
      .then(() => {
        toast(`Created ${task.name}`, 4000);
        this.selectedUser = null;
        taskForm.reset();
      })
      .catch(error => {
        toast('Failed to create task', 4000);
        console.error('task creation error', error);
      });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }
}
