import { Component, OnInit } from '@angular/core';
import { TasksService } from '../tasks.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentSnapshot } from '@firebase/firestore-types';

@Component({
  selector: 'app-tasks-detail',
  templateUrl: './tasks-detail.component.html',
  styleUrls: ['./tasks-detail.component.css']
})
export class TasksDetailComponent implements OnInit {
  task;

  constructor(private tasksService: TasksService,
              private route: ActivatedRoute,
              private router: Router) {
    this.task = null;
  }

  ngOnInit() {
    this.route.paramMap
      .subscribe(params => {
        const projectId = params.get('projectId');
        const taskId = params.get('taskId');
        if (!projectId || ! taskId) {
          console.error('task detail - no project id and/or no task id');
          this.router.navigate(['../'], { relativeTo: this.route });
        }
        this.tasksService.getTaskByProjectIdAndTaskId(projectId, taskId)
          .then((snapshot: DocumentSnapshot) => {
            if (!snapshot.exists) {
              console.error('task detail - could not find task');
              this.router.navigate(['../'], { relativeTo: this.route });
            }
            const id = snapshot.id;
            const docData = snapshot.data();
            this.task = { id, ...docData };
          })
          .catch(error => {
            console.error('task detail error', error);
            this.router.navigate(['../'], { relativeTo: this.route });
          });
      });
  }
}
