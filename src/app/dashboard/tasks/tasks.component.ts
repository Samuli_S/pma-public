import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../core/auth.service';
import { TasksService, Task } from './tasks.service';
import { User } from '../users/users.service';
import { QuerySnapshot } from '@firebase/firestore-types';
import { ProjectsService } from '../projects/projects.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit, OnDestroy {
  user: User;
  usersSubscription: Subscription;
  isLoadingTasks: boolean;
  tasksExist: boolean;
  currentProjectId: string;
  tasks: Task[];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private tasksService: TasksService,
              private authService: AuthService,
              public projectsService: ProjectsService) {
    this.tasks = [];
    this.currentProjectId = null;
    this.isLoadingTasks = true;
    this.tasksExist = true;
  }

  ngOnInit() {
    this.usersSubscription = this.authService.user
      .subscribe(user => {
        this.user = user;
      });
    setTimeout(() => {
      this.route.paramMap
      .subscribe(params => {
        this.currentProjectId = params.get('projectId');
        this.getProjectTasks();
      });
    }, 2000);
  }

  getProjectTasks() {
    if (this.user.userGroup === 'owner' || this.user.userGroup === 'manager') {
      this.getAllProjectTasks();
    } else {
      this.getProjectUserTasks();
    }
  }

  getProjectUserTasks() {
    this.isLoadingTasks = true;
    this.tasksService.listTasksByProjectIdAndAssigneeUID(this.currentProjectId, this.user.uid)
      .then((snapshot: QuerySnapshot) => {
        if (snapshot.size === 0) {
          this.tasksExist = false;
          this.isLoadingTasks = false;
          return;
        }
        this.tasks = [];
        snapshot.docs.forEach(doc => {
          const docData = doc.data();
          const id = doc.id;
          const task = { id, ...docData };
          this.tasks.push(task as Task);
          this.isLoadingTasks = false;
        });
      })
      .catch(error => {
        console.error('error getting tasks', error);
        this.isLoadingTasks = false;
      });
  }

  getAllProjectTasks() {
    this.isLoadingTasks = true;
    this.tasksService.listTasksByProjectId(this.currentProjectId)
      .then((snapshot: QuerySnapshot) => {
        if (snapshot.size === 0) {
          this.tasksExist = false;
          this.isLoadingTasks = false;
          return;
        }
        this.tasks = [];
        snapshot.docs.forEach(doc => {
          const docData = doc.data();
          const id = doc.id;
          const task = { id, ...docData };
          this.tasks.push(task as Task);
          this.isLoadingTasks = false;
        });
      })
      .catch(error => {
        console.error('error getting tasks', error);
        this.isLoadingTasks = false;
      });
  }

  onTaskSelected(task: Task) {
    this.router.navigate(['/dashboard', { outlets: { dashboard: ['projects', this.currentProjectId, 'tasks', task.id] } }]);
  }

  ngOnDestroy() {
    this.usersSubscription.unsubscribe();
  }
}
